## 陌路代码生成器

代码介绍地址：[手把手教你如何使用SpringBoot3打造一个个性化的代码生成器](https://blog.csdn.net/qq_51076413) 	https://blog.csdn.net/qq_51076413/article/details/135394070

CSDN主页：[.陌路](https://blog.csdn.net/qq_51076413)  https://blog.csdn.net/qq_51076413

### 本地启动

### 一、修改配置文件

```yml
# 配置生成代码的数据库
cn:
  molu:
    generate:
      database: temp1 # 修改需要连接的数据库名称
      
spring: 
  # 数据源配置
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    # 修改数据库
    url: jdbc:mysql://127.0.0.1:3306/temp1?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&autoReconnect=true&allowMultiQueries=true
    username: xxx #修改用户名
    password: xxx # 修改密码
```

### 二、修改JDK

> `File` -> `Project Structure` -> `Project Settings` -> `Project` -> `SDK` 选择JDK17

如果没有`JDK17`需要下载

下载步骤：

> `File` -> `Project Structure` -> `Project Settings` -> `SDKs` -> `+` 号 -> `Download JDK`  -> `version选择版本号` -> `选择安装目录下载即可`下载完成后以上步骤修改`JDK`版本

### 三、启动项目

> 找到 `MoluGeneratorApplication.java`  启动类，点击运行即可启动

### 四、访问首页

> 项目启动后，在控制台中找到最后的 `项目启动成功` 访问地址点击访问即可

```java
2024-01-17T18:59:57.205+08:00  INFO 25100 --- [  restartedMain] o.s.b.a.w.s.WelcomePageHandlerMapping    : Adding welcome page: class path resource [templates/index.html]
2024-01-17T18:59:57.594+08:00  WARN 25100 --- [  restartedMain] o.s.b.d.a.OptionalLiveReloadServer       : Unable to start LiveReload server
2024-01-17T18:59:57.671+08:00  INFO 25100 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8989 (http) with context path ''
2024-01-17T18:59:57.692+08:00  INFO 25100 --- [  restartedMain] c.m.generator.MoluGeneratorApplication   : Started MoluGeneratorApplication in 7.104 seconds (process running for 9.295)
==============项目启动成功！==============
请访问地址：http://192.168.20.102:8989
=======================================
```

### 五、使用步骤

**说明：**

> 1、表名：项目启动后，访问页面时，后台将会访问之前配置好的 `temp1` 数据库，并将数据库中的所有表信息进行提取
>
> 2、代码类型：代码类型可以配置，需要生成哪些代码，配置完成之后需要在 `resource/vm` 目录下进行添加完善
>
> **本demo示例是将需要生成的类型写到了 `JS` 的 `codeType` 变量中，可根据自己需要进行修改或配置到数据库，访问页面时从后台读取即可**

**步骤：**

> 1、选择表名【必须】：选择所要操作的表，根据表去后台提取表结构内容
>
> 2、选择类型【必须】：选择需要生成的代码类型eg：`Java`代码、`vue`代码、`xml`代码等
>
> 3、删除前缀【选填】：是否需要删除表的前缀eg：表 `tb_user` 填入 `tb_` 则生成的代码会是 `User`否则是 `TbUser`
>
> 4、文件包名【选填】：生成的代码所在的包名，eg：`cn.molu.generator` 那么生成的代码所在的包会是 `package cn.molu.generator.entity`
>
> 5、生成代码：以上 `4` 步完成之后，点击生成代码，下方框中将会显示生成后的代码

### 六、开源说明

代码介绍地址：[手把手教你如何使用SpringBoot3打造一个个性化的代码生成器](https://blog.csdn.net/qq_51076413) 	https://blog.csdn.net/qq_51076413/article/details/135394070

CSDN主页：[.陌路](https://blog.csdn.net/qq_51076413)  https://blog.csdn.net/qq_51076413

**说明：**

> ==本demo仅供学习参考使用==
>
> ==本demo未使用 `vue` 代码做演示，此代码为项目抽离代码，无法提供完整代码==

更多技术分享请关注：[.陌路-CSDN博客](https://blog.csdn.net/qq_51076413?type=blog)

**创作不易，采集、转发请注明出处：https://blog.csdn.net/qq_51076413?type=blog**



